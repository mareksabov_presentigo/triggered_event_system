# Triggered event system

- This is plugin from PresentiGO.
- This plugin improved using animation events.

## Usage:
- Right click in editor hierarchy and add TriggeredEventSystem.
- In inspector window add events.
- Add TirggeredEventThrower component to the object, where the animation is.
- In animation window add Animation event and assign function ThrowEventInt(int id) or ThrowEventString(string name).

#ChangeLog:

### 1.0.2
- Fixed problem with showing details of event.

### 1.0.1
- Added CollisionTriggeredEventThrower. This component throws event when some collider hit trigger with this component.

### 1.0.0
- First released version.
