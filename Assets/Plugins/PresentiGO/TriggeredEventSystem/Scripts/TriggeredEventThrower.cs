﻿using UnityEngine;

namespace PresentiGO.TriggeredEventSystem
{
    /// <summary>
    /// This component is used for throwing event from animation.
    /// Usage: Add this component to the object, where the animation is and call <see cref="ThrowEventInt(int)"/> or <see cref="ThrowEventString(string)"/> function.
    /// </summary>
    public class TriggeredEventThrower : MonoBehaviour
    {
        /// <summary>
        /// Function for throwing triggered event by id of the event.
        /// </summary>
        /// <param name="id">ID of the event.</param>
        public void ThrowEventInt(int id)
        {
            TriggeredEventSystem.ThrowEventStatic(id);
        }

        /// <summary>
        /// Function for throwing triggered event by name of the event.
        /// </summary>
        /// <param name="name">Name of the event.</param>
        public void ThrowEventString(string name)
        {
            TriggeredEventSystem.ThrowEventStatic(name);
        }
    }
}
