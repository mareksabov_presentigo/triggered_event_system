﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace PresentiGO.TriggeredEventSystem
{
    /// <summary>
    /// Class for throwing event from collider.
    /// Usage:
    /// </summary>
    [RequireComponent(typeof(BoxCollider))]
    [RequireComponent(typeof(Rigidbody))]
    public class CollisionTriggeredEventThrower : MonoBehaviour
    {
        private BoxCollider m_BoxCollider;

        [Header("Event")]
        /// <summary>
        /// Id of the event.
        /// </summary>
        [SerializeField, Tooltip("Id of the event. Needs to be setted up if is used throwing by id.")]
        private int m_Id = -1;
        /// <summary>
        /// Name of the event.
        /// </summary>
        [SerializeField, Tooltip("Name of the event. Needs to be setted up if is used throwing by name.")]
        private string m_EventName = "";

        [Header("Settings")]
        /// <summary>
        /// If true, event will be throwed by name, else event will be throwed by id.
        /// </summary>
        [SerializeField, Tooltip("If true, event will be throwed by name, else event will be throwed by id.")]
        private bool m_ThrowByName = true;
        /// <summary>
        /// If true, event will be throwed OnTriggerEnter.
        /// </summary>
        [SerializeField, Tooltip("If true, event will be throwed OnTriggerEnter.")]
        private bool m_ThrowOnTriggerEnter = true;
        /// <summary>
        /// If true, event will be throwed OnTriggerExit.
        /// </summary>
        [SerializeField, Tooltip("If true, event will be throwed OnTriggerExit.")]
        private bool m_ThrowOnTriggerExit = false;

        /// <summary>
        /// If true, event will be throwed only if object is in layer mask.
        /// </summary>
        [SerializeField, Tooltip("If true, event will be throwed only if object is in layer mask.")]
        private bool m_UseLayerMask = false;
        /// <summary>
        /// If is object hitting trigger in layermask, throw event.
        /// </summary>
        [SerializeField, Tooltip("If is object hitting trigger in layermask, throw event.")]
        private LayerMask m_LayerMask;
        
        /// <summary>
        /// Unity Awake() function.
        /// <see cref="m_BoxCollider"/> is assigned here.
        /// </summary>
        private void Awake()
        {
            m_BoxCollider = GetComponent<BoxCollider>();
            m_BoxCollider.isTrigger = true;
        }

        /// <summary>
        /// Checking if throw event of trigger enter.
        /// </summary>
        /// <param name="other">Collider cilliding with this trigger.</param>
        private void OnTriggerEnter(Collider other)
        {
            if (m_ThrowOnTriggerEnter)
            {
                if (m_UseLayerMask)
                {
                    if (m_LayerMask == (m_LayerMask | (1 << other.gameObject.layer)))
                    {
                        if (m_ThrowByName)
                            TriggeredEventSystem.ThrowEventStatic(m_EventName);
                        else
                            TriggeredEventSystem.ThrowEventStatic(m_Id);
                    }
                   
                }
                else
                {
                    if (m_ThrowByName)
                        TriggeredEventSystem.ThrowEventStatic(m_EventName);
                    else
                        TriggeredEventSystem.ThrowEventStatic(m_Id);
                }
            }
        }

        /// <summary>
        /// Checking if throw event of trigger exit.
        /// </summary>
        /// <param name="other">Collider cilliding with this trigger.</param>
        private void OnTriggerExit(Collider other)
        {
            if (m_ThrowOnTriggerExit)
            {
                if (m_UseLayerMask)
                {
                    if (m_LayerMask == (m_LayerMask | (1 << other.gameObject.layer)))
                    {
                        if (m_ThrowByName)
                            TriggeredEventSystem.ThrowEventStatic(m_EventName);
                        else
                            TriggeredEventSystem.ThrowEventStatic(m_Id);
                    }

                }
                else
                {
                    if (m_ThrowByName)
                        TriggeredEventSystem.ThrowEventStatic(m_EventName);
                    else
                        TriggeredEventSystem.ThrowEventStatic(m_Id);
                }
            }
        }
    }
}
