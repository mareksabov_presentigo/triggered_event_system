﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Namespace collecting whole Triggered event system
/// </summary>
namespace PresentiGO.TriggeredEventSystem
{
    /// <summary>
    /// Base abstract class for triggered events
    /// </summary>
    [Serializable]
    public abstract class BaseEvent
    {
        /// <summary>
        /// Name of the event.
        /// </summary>
        [SerializeField]
        public string Name;

        /// <summary>
        /// Name of the event assigned from <see cref="TriggeredEventSystem"/> in format Event{id}.
        /// </summary>
        [SerializeField]
        public string OldName;

        /// <summary>
        /// ID of the event.
        /// </summary>
        [SerializeField]
        public int ID;

        /// <summary>
        /// Type of the event.
        /// </summary>
        [SerializeField]
        public EventType Type;

        /// <summary>
        /// Function for invocation of the event.
        /// </summary>
        public abstract void Invoke();
    }
}
