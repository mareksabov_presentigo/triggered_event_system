﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace PresentiGO.TriggeredEventSystem
{
    /// <summary>
    /// Event cooperating with UnityEvent.
    /// This event uses UnityEvent and in Invoke() function will be this UnityEvent invoked.
    /// </summary>
    [Serializable]
    public class TriggeredUnityEvent : BaseEvent 
    {
        /// <summary>
        /// UnityEvent
        /// </summary>
        [SerializeField] public UnityEvent Event;
        /// <summary>
        /// Override function for invokation.
        /// If <see cref="Event"/> is not null, it will be invoked here.
        /// </summary>
        public override void Invoke()
        {
            Event?.Invoke();
        }
    }
}
