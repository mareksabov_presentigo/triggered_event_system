﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace PresentiGO.TriggeredEventSystem
{
    /// <summary>
    /// Class holding Event properties.
    /// </summary>
    [Serializable]
    public class EventProperties
    {
        /// <summary>
        /// Unity event.
        /// </summary>
        [SerializeField]
        public UnityEvent UnityEvent;
        /// <summary>
        /// ID of the event.
        /// </summary>
        [SerializeField]
        public int ID;
    }
}
