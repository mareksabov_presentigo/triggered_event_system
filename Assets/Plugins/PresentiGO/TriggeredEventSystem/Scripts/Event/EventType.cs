﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PresentiGO.TriggeredEventSystem
{
    /// <summary>
    /// Enum holdings event types.
    /// </summary>
    public enum EventType
    {
        UNITY_EVENT,
        ACTION
    }
}
