﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PresentiGO.TriggeredEventSystem
{
    /// <summary>
    /// This class contatins whole function of Triggered event system.
    /// This is class also has editor script (<seealso cref="TriggeredEventSystemEditor"/>).
    /// This class derive from <see cref="Utilities.Singleton{T}"/>
    /// </summary>
    [Serializable]
    public class TriggeredEventSystem : Utilities.Singleton<TriggeredEventSystem>
    {
        /// <summary>
        /// List of triggered events.
        /// </summary>
        [SerializeField]
        public List<TriggeredUnityEvent> Events = new List<TriggeredUnityEvent>();

        /// <summary>
        /// List of UnityEvents.
        /// </summary>
        [SerializeField]
        public List<EventProperties> UEvents = new List<EventProperties>();

        /// <summary>
        /// Actual Id of new event.
        /// </summary>
        [SerializeField]
        public int ID = 0;

        /// <summary>
        /// Function for throwing event by id.
        /// </summary>
        /// <param name="id">ID of the event.</param>
        public void ThrowEvent(int id)
        {
            if (Events != null && id >= 0)
            {
                var _event = Events.FirstOrDefault(x => x.ID == id);

                if (_event != null)
                {
                    _event.Invoke();
                }
            }
        }

        /// <summary>
        /// Function for throwing event by name.
        /// </summary>
        /// <param name="name">Name of the event.</param>
        public void ThrowEvent(string name)
        {
            var _event = Events?.FirstOrDefault(x => x.Name == name);

            ThrowEvent(_event != null ? _event.ID : -1);
        }

        /// <summary>
        /// Static function for throwing event by id.
        /// </summary>
        /// <param name="id">ID of the event.</param>
        public static void ThrowEventStatic(int id)
        {
            Instance.ThrowEvent(id);
        }

        /// <summary>
        /// Static function for throwing event by id.
        /// </summary>
        /// <param name="name">Name of the event.</param>
        public static void ThrowEventStatic(string name)
        {
            Instance.ThrowEvent(name);
        }

    }
}
