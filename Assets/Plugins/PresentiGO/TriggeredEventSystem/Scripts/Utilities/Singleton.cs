﻿using System;
using UnityEngine;

namespace PresentiGO.TriggeredEventSystem.Utilities
{
    /// <summary>
    /// Class for MonoBehaviour singleton.
    /// </summary>
    /// <typeparam name="T">Parametric type of class, which devied from this singleton.</typeparam>
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static bool SINGLETON_DEBUG;

        private bool eventRegistered;
        private static bool isQuit = false;
        protected static T m_Instance;

        /// <summary>
        /// Property, if schould be instance destroyed on load or not.
        /// </summary>
        public bool DontDestroy
        {
            get
            {
                return gameObject.GetComponent<DontDestroyOnLoad>() != null;
            }
            set
            {
                if (!value && DontDestroy)
                    GameObject.Destroy(GetComponent<DontDestroyOnLoad>());

                if (value && !DontDestroy)
                    gameObject.AddComponent<DontDestroyOnLoad>();
            }
        }

        /// <summary>
        /// Static getter of Instance.
        /// Returns instance saved in m_Instance.
        /// </summary>
        public static T Instance
        {
            get
            {
                if (m_Instance == null && !isQuit)
                {
                    m_Instance = (T)FindObjectOfType(typeof(T));

                    if (m_Instance == null)
                    {
                        GameObject go = new GameObject(typeof(T).Name);
                        m_Instance = go.AddComponent<T>();

                        //Debug.LogError("An instance of " + typeof(T) +
                        //   " is needed in the scene, but there is none.");
                    }
                }

                return m_Instance;
            }
        }

        /// <summary>
        /// Static bool Getter, return true if instance is created, else returns false.
        /// </summary>
        public static bool IsCreated
        {
            get { return m_Instance != null; }
        }

        /// <summary>
        /// Function for clear instace.
        /// This functoin fill find and destroy game object of instance.
        /// </summary>
        public void ClearInstances()
        {
            UnityEngine.Object[] instances = FindObjectsOfType(typeof(T));

            if (instances.Length > 1 && m_Instance != this)
            {
                Destroy(gameObject);
            }
        }

        /// <summary>
        /// Unity Awake() function.
        /// Initialization is here.
        /// </summary>
        public virtual void Awake()
        {
            if (!IsCreated)
                m_Instance = Instance;

            if (SINGLETON_DEBUG)
                Debug.Log(String.Format("{0}.Awake()", typeof(T).Name));

            if (!eventRegistered)
                RegisterEvents();
        }

        /// <summary>
        /// Unity OnApplicationQuit() function.
        /// </summary>
        public virtual void OnApplicationQuit()
        {
            isQuit = true;
        }

        /// <summary>
        /// Unity OnDestroy() function.
        /// Deinitialization is here.
        /// </summary>
        public virtual void OnDestroy()
        {
            //if (SINGLETON_DEBUG && DebugConsole.IsCreated)
            //  DebugConsole.Log(String.Format("{0}.OnDestroy()", typeof(T).Name));

            if (!isQuit)
            {
                if (eventRegistered)
                    UnRegisterEvents();
            }

            m_Instance = null;
        }

        /// <summary>
        /// Function for registring events.
        /// </summary>
        protected virtual void RegisterEvents()
        {
            if (SINGLETON_DEBUG)
                Debug.Log(String.Format("{0}.RegisterEvents()", typeof(T).Name));

            eventRegistered = true;
        }

        /// <summary>
        /// Function for unregistring events.
        /// </summary>
        protected virtual void UnRegisterEvents()
        {
            //if (SINGLETON_DEBUG && DebugConsole.IsCreated)
            //  DebugConsole.Log(String.Format("{0}.UnRegisterEvents()", typeof(T).Name));

            eventRegistered = false;
        }
    }
}

