﻿using UnityEngine;

/// <summary>
/// Namespace collecting utilities for triggered event system.
/// </summary>
namespace PresentiGO.TriggeredEventSystem.Utilities
{
    public class DontDestroyOnLoad : MonoBehaviour
    {
        /// <summary>
        /// Awake method. Here is calling of the <see cref="DontDestroyOnLoad()"/> function.
        /// </summary>
        private void Awake()
        {
            DontDestroyOnLoad(this);
        }
    }
}
