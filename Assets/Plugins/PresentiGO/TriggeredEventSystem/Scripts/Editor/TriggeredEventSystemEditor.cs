﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

namespace PresentiGO.TriggeredEventSystem
{
    /// <summary>
    /// Editor script for <see cref="TriggeredEventSystem"/>.
    /// Code of custom editor is here.
    /// </summary>
    [CustomEditor(typeof(TriggeredEventSystem))]
    [CanEditMultipleObjects]
    [Serializable]
    public class TriggeredEventSystemEditor : Editor
    {
        /// <summary>
        /// Dictionaty of boolss, if is details for the event showed or not.
        /// Key: ID of the event.
        /// Value: bool is is details showed.
        /// </summary>
        private static Dictionary<int, bool> m_ShowDetail = new Dictionary<int, bool>(); // eventID, show
        /// <summary>
        /// List of evet ids, which will be removed at the end of frame.
        /// </summary>
        private List<int> m_ToRemove = new List<int>();
        /// <summary>
        /// GUI Style
        /// </summary>
        private GUIStyle m_GUIStyle = new GUIStyle();
       //private int id = 0;

        /// <summary>
        /// Triggered event system.
        /// This is target for custom editor.
        /// </summary>
        TriggeredEventSystem m_EventSystem;

        /// <summary>
        /// Awake function. <see cref="m_EventSystem"/> is assigned here.
        /// </summary>
        private void Awake()
        {
            m_EventSystem = target as TriggeredEventSystem;
        }

        /// <summary>
        /// Overrided function for drawing inscpector.
        /// </summary>
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            foreach (var e in m_EventSystem.Events)
            {
                ShowEvent(e);
            }

            if (GUILayout.Button(new GUIContent("+","Add new event.")))
            {
                var name = $"Event{m_EventSystem.ID}";
                var e = new TriggeredUnityEvent { ID = m_EventSystem.ID, Name = name, OldName = name, Event = null };
                AddEvent(e);
                m_EventSystem.ID++;
            }

            RemoveEvents();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(m_EventSystem);
            }
        }

        /// <summary>
        /// Function for adding new event.
        /// </summary>
        /// <param name="e">New event</param>
        private void AddEvent(TriggeredUnityEvent e)
        {
            m_EventSystem.Events.Add(e);

            var prop = m_EventSystem.UEvents.FirstOrDefault(x => x.ID == e.ID);
            if (prop == null)
            {
                m_EventSystem.UEvents.Add(new EventProperties { ID = e.ID, UnityEvent = e.Event });
            }

        }

        /// <summary>
        /// Function for showing event.
        /// </summary>
        /// <param name="e">This event will be showed.</param>
        private void ShowEvent(TriggeredUnityEvent e)
        {
            if (!m_ShowDetail.ContainsKey(e.ID))
                m_ShowDetail.Add(e.ID, false);

            GUILayout.BeginHorizontal();
            m_ShowDetail[e.ID] = EditorGUILayout.Foldout(m_ShowDetail[e.ID], new GUIContent($"{e.Name} (ID: {e.ID})", $"Informations about { e.Name } (ID: { e.ID}) event."));
            if (GUILayout.Button(new GUIContent("-", "Delete this event."), GUILayout.Width(25f)))
            {
                RemoveEvent(e);
            }
            GUILayout.EndHorizontal();

            if (m_ShowDetail[e.ID])
            {
                EditorGUILayout.IntField(new GUIContent("ID","ID of the event."), e.ID);
                var text = EditorGUILayout.TextField(new GUIContent("Name", "Name of the event."), e.Name);
                //var type = (EventType)EditorGUILayout.EnumPopup("Type", e.Type);


                if (e.Type == EventType.UNITY_EVENT)
                {
                    SerializedProperty uevent = serializedObject.FindProperty("UEvents");

                    var properties = m_EventSystem?.UEvents?.FirstOrDefault(x => x.ID == e.ID);
                    var index = m_EventSystem?.UEvents?.IndexOf(properties);

                    if (index != null)
                    {
                        int i = index ?? 0;
                        var prop = uevent.GetArrayElementAtIndex(i);

                        if(prop!=null)
                        {
                            EditorGUILayout.PropertyField(prop.FindPropertyRelative("UnityEvent"), new GUIContent("", "Unity Event"));
                        }
                        var _event = e as TriggeredUnityEvent;

                        _event.Event = properties.UnityEvent;
                    }

                }

                #region AsignNewVariables
                e.Name = SetCorrenctName(e, text);
                //e.Type = type;
                #endregion

                serializedObject.ApplyModifiedProperties();
            }


        }

        /// <summary>
        /// Function for removing event.
        /// This function fill add event to <see cref="m_ToRemove"/>.
        /// </summary>
        /// <param name="e">This event will be removed.</param>
        private void RemoveEvent(TriggeredUnityEvent e)
        {
            if (!m_ToRemove.Contains(e.ID))
                m_ToRemove.Add(e.ID);
        }

        /// <summary>
        /// Function for removing all events in <see cref="m_ToRemove"/>.
        /// </summary>
        private void RemoveEvents()
        {
            foreach (var id in m_ToRemove)
            {
                var e = m_EventSystem.Events.FirstOrDefault(x => x.ID == id);
                if (e != null)
                {
                    m_EventSystem.Events.Remove(e);
                    if (m_ShowDetail.ContainsKey(e.ID))
                        m_ShowDetail.Remove(e.ID);
                }

                var ue = m_EventSystem.UEvents.FirstOrDefault(x => x.ID == id);
                if (ue != null)
                    m_EventSystem.UEvents.Remove(ue);
            }

            m_ToRemove = new List<int>();
        }

        /// <summary>
        /// Function for create new Tirggered event system.
        /// </summary>
        [MenuItem("GameObject/TriggeredEventSystem", false, 49)]
        private static void Create()
        {
            GameObject go = FindObjectOfType<TriggeredEventSystem>()?.gameObject;

            if(go == null)
            {
                go = new GameObject("TriggeredEventSystem");
                go.AddComponent<TriggeredEventSystem>();
                go.transform.position = Vector3.zero;
                go.transform.localScale = Vector3.one;
                go.transform.rotation = Quaternion.identity;
            }

            Selection.objects = new GameObject[1] { go };
        }

        /// <summary>
        /// Function for getting correct name of the event.
        /// If new name exits, it will returns base name and error to log console.
        /// </summary>
        /// <param name="e">Event</param>
        /// <param name="newName">New name for the Event</param>
        /// <returns>Name of the event.</returns>
        private string SetCorrenctName(TriggeredUnityEvent e, string newName)
        {
            if(e.Name != newName)
            {
                var objWithName = m_EventSystem.Events?.FirstOrDefault(x => x.Name == newName);

                if (objWithName == null)
                {
                    return newName;
                }
                else
                {
                    Debug.LogError($"Event with name \"{newName}\" already exists.");
                    return e.OldName;
                }
            }
            else
            {
                return newName;
            }
        }
    }
}
